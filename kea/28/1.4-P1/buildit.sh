#!/bin/bash

docker build -t kea . $DOCKER_BUILD_ARGS
docker run -v "`pwd`/../../../build:/build" kea
