#!/bin/bash

docker build -t firewalld . $DOCKER_BUILD_ARGS
docker run -v "`pwd`/../../../build:/build" firewalld
