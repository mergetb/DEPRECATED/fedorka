#!/bin/bash

docker build -t protobuf . $DOCKER_BUILD_ARGS
docker run -v "`pwd`/../../build:/build" protobuf
